class Paddle {
    constructor() {
        this.position = [150, 380]
        this.height = 10
        this.width = 50
    }

    render(ctx) {
        ctx.fillStyle = "black";
        ctx.fillRect(this.position[0], this.position[1], this.width, this.height);

    }

    move() {
        document.addEventListener('keydown', (event) => {
            if (event.keyCode === 39) {
                this.position[0] += 0.03;
            }
            if (event.keyCode === 37) {
                this.position[0] -= 0.03;
            }
        })
    }
}


