class Block {
    constructor([Xpos, Ypos], color) {
        this.position = [Xpos, Ypos]
        this.height = 15
        this.width = 20
        this.color = color
    }

    render(ctx) {
        ctx.fillStyle = this.color;
        ctx.fillRect(this.position[0], this.position[1], this.width, this.height);
    }
}