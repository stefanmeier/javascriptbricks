## JavaScript Bricks
  
This code is only for people who want to see my very early code.
  
A replica of the bricks game using Canvas (HTML object for rendering in 2D with JavaScript).  
This is an exercise from the first week at Propulsion Academy.  
  
The game is not finished, as the bricks are not destroyed and the paddle goes faster the more you use it.  
To try it out just put the HTML and the JS files in the same Directory and open the HTML with a browser.
The Paddles is controlled with left and right arrow keys.  