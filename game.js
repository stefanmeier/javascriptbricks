class Game {
    constructor() {
        this.canvas = document.querySelector('#canvas');
        this.ctx = this.canvas.getContext('2d');
        this.a = new Ball([100, 120]);
        this.blocks = []
        this.paddle = new Paddle;
    }

    createBlock(line) {
        let a = 15
        let b = 15
        for (let i = 0; i < line; i++) {
            for (let i = 0; i < 13; i++) {
                this.blocks.push(new Block([a, b], this.getColor()))
                a += 21
            }
            b += 20
            a = 15
        }
    }

    resetCanvas() {
        this.ctx.fillStyle = 'grey';
        this.ctx.fillRect(0, 0, 300, 400);
    }

    play() {
        this.createBlock(5);
        setInterval(() => {
            this.resetCanvas();
            this.checkCollisions();
            this.a.move();
            this.paddle.move();
            this.a.render(this.ctx);
            this.paddle.render(this.ctx);
            this.blocks.forEach(element => element.render(this.ctx));
        }, 17);
    }

    getColor() {
        let letters = '0123456789ABCDEF';
        let color = "#";
        for (let i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    checkCollisions() {
        for (let i = 0; i < this.blocks.length - 1; i++) {
            if (this.areColliding(this.blocks[i], this.a)) {
                alert("f")
                console.log("collided")
            }
        }
    }

    areColliding(i, ball) {
        let lx = i.position[0]
        let rx = i.position[0] + this.width
        let uy = i.position[1] + this.height
        let oy = i.position[1]
        let bx = ball.position[0]
        let by = ball.position[1]
        return bx + ball.radius > lx
            && bx - ball.radius < rx
            && by + ball.radius > oy
            && by - ball.radius < uy;
    }
}
