class Ball {
    constructor([Xposition, Yposition]) {
        this.position = [Xposition, Yposition]
        this.velocity = [-5, -5];
        this.radius = 5
    }

    render(ctx, ) {
        ctx.beginPath();
        ctx.arc(this.position[0], this.position[1], this.radius, 0, 10 * Math.PI, false);
        ctx.fillStyle = "white";
        ctx.fill();
    }

    move() {
        if (this.position[1] <= 0) {
            this.velocity[1] *= -1;
        }
        if (this.position[1] >= 400) {
            this.velocity[1] *= -1
        }
        if (this.position[0] <= 0) {
            this.velocity[0] *= -1
        }
        if (this.position[0] >= 300) {
            this.velocity[0] *= -1
        }
        this.position[0] += this.velocity[0];
        this.position[1] += this.velocity[1];
    }
}